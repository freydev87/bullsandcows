import React, { useEffect, useState, useCallback, useMemo } from 'react';
import styles from './App.module.scss';
import buttonStyles from './Button.module.scss';


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function History({ history }) {
  return (
    <div className={styles.history}>
      <h4>История:</h4>
      {history.map(elem =>
        <p className={styles.historyItem} key={elem.turn}>
          <div>
            Ход: <strong>&nbsp;{elem.turn}</strong>
          </div>
          <div>
            Число: <strong>&nbsp;{elem.number}</strong>
          </div>
          <div>
            <i className={styles.cow} />: {elem.cows}
          </div>
          <div>
            <i className={styles.bull} />: {elem.bulls}
          </div>
        </p>
      )}
    </div>
  )
}


const checkNumber = (playerNumber, number) => {
  let cows = 0;
  let bulls = 0;

  const numberArray = playerNumber.split('');
  for (let [index, char] of numberArray.entries()) {
    if (number[index] == char) { bulls++ }
    else if (~number.indexOf(char)) { cows++ }
  }

  return { cows, bulls }
}

function Game({ number, handleRestart }) {
  const [playerNumber, setPlayerNumber] = useState('');
  const [history, setHistory] = useState([]);
  const [turn, setTurn] = useState(1);
  const [error, showError] = useState(false);
  const [win, setWin] = useState(false);

  const handleChange = useCallback(e => {
    const number = parseInt(e.target.value);
    setPlayerNumber(e.target.value)

    const invalidNumber = new Set(e.target.value.split('')).size !== 4
    if (isNaN(number) || invalidNumber) {
      showError(true)
      return
    }

    showError(false)
  }, [])

  const handleSubmit = useCallback(e => {
    e.preventDefault();
    if (error) return

    setTurn(prevTurn => prevTurn + 1)
    const result = checkNumber(playerNumber, number)

    setHistory(prev => prev.concat({
      turn: turn,
      number: playerNumber,
      ...result
    }))

    if (result.bulls === 4) {
      setWin(true)
    }

  }, [playerNumber, turn])

  const onHandleRestart = useCallback(() => {
    setHistory([])
    setPlayerNumber('')
    setWin(false)
    setTurn(1)

    handleRestart()
  }, [])

  return (
    <div className={styles.game}>
      <History history={history} />
      <form onSubmit={handleSubmit} className={styles.gameForm}>
        <label htmlFor="number">Ход: {turn}</label>
        <input disabled={win} id="number" maxLength={4} name="playerNumber" value={playerNumber} onChange={handleChange} type="text" className={styles.gameInput} />
        {error && <div className={styles.error}>Введите правильное число</div>}
        {win && <>
          <div className={styles.win}>Winner!</div>
          <Button className={styles.button} onClick={onHandleRestart}>Начать заного</Button>
        </>}
      </form>
    </div>
  )
}


const Button = ({ children, ...props }) => (
  <button {...props} className={buttonStyles.btn}>{children}</button>
)

function App() {
  const [isStarted, start] = useState(false);
  const [number, setNumber] = useState(false);

  const generateNumber = () => {
    let n = String(getRandomInt(9));

    while (n.length < 4) {
      const newN = getRandomInt(9);
      if (n.indexOf(newN) == -1) { n += newN }
    }

    return n
  }

  const handleStart = useCallback(() => {
    setNumber(generateNumber())
    start(true)
  })

  const handleRestart = useCallback(() => {
    setNumber(generateNumber())
  }, [])

  return (
    <div className={styles.container}>
      <div className={styles.mainForm}>
        <h3 className={styles.header}>Быки и Коровы</h3>
        {!isStarted && <Button onClick={handleStart}>Начать игру</Button>}
        {isStarted && <Game handleRestart={handleRestart} number={number} />}
      </div>
    </div>
  );
}

export default App;
